package com.nesterov.tasktracker.repository;

import com.nesterov.tasktracker.entity.CommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<CommentEntity, Long> {
}
