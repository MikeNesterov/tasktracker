package com.nesterov.tasktracker.repository;

import com.nesterov.tasktracker.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByEmail(String login);
}
