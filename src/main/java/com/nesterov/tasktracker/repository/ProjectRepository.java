package com.nesterov.tasktracker.repository;

import com.nesterov.tasktracker.entity.ProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface ProjectRepository extends JpaRepository<ProjectEntity, Long> {

    Set<ProjectEntity> getAllByManager_Id(long id);
}
