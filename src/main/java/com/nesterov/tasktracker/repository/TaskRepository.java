package com.nesterov.tasktracker.repository;

import com.nesterov.tasktracker.entity.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<TaskEntity, Long> {
}
