package com.nesterov.tasktracker.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "project")
public class ProjectEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private UserEntity manager;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "project", cascade = CascadeType.PERSIST)
    @JsonIgnore
    private List<TaskEntity> tasks;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "projects")
    @JsonIgnore
    private Set<UserEntity> developers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserEntity getManager() {
        return manager;
    }

    public void setManager(UserEntity manager) {
        this.manager = manager;
    }

    public List<TaskEntity> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskEntity> tasks) {
        this.tasks = tasks;
    }

    public Set<UserEntity> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Set<UserEntity> developers) {
        this.developers = developers;
    }
}
