package com.nesterov.tasktracker.service;

import com.nesterov.tasktracker.entity.TokenEntity;
import com.nesterov.tasktracker.entity.UserEntity;

import java.util.List;

public interface UserService {

    List<UserEntity> getAll();
    UserEntity getById(long id);
    UserEntity save(UserEntity user);
    UserEntity saveNewUser(UserEntity user);
    void delete(long id);
    UserEntity getByEmail(String email);
    void confirmRegistration(TokenEntity token);
    boolean checkUserInDB(String login);

}
