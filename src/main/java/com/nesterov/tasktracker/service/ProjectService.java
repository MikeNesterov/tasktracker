package com.nesterov.tasktracker.service;

import com.nesterov.tasktracker.entity.ProjectEntity;

import java.util.Set;

public interface ProjectService {
    Set<ProjectEntity> getAllByManager(long user_id);
}
