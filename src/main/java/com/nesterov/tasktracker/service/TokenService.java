package com.nesterov.tasktracker.service;

import com.nesterov.tasktracker.entity.TokenEntity;
import com.nesterov.tasktracker.entity.UserEntity;

public interface TokenService {

    TokenEntity save(TokenEntity token);
    TokenEntity getToken(String token);
    void delete(TokenEntity token);
    TokenEntity createToken(UserEntity user);
    boolean validateToken(TokenEntity token);
}
