package com.nesterov.tasktracker.service.Impl;

import com.nesterov.tasktracker.entity.TokenEntity;
import com.nesterov.tasktracker.entity.UserEntity;
import com.nesterov.tasktracker.repository.TokenRepository;
import com.nesterov.tasktracker.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Service
@Transactional
@PropertySource("classpath:messages.properties")
public class TokenServiceImpl implements TokenService {

    @Resource
    private Environment env;

    @Autowired
    private TokenRepository repository;

    @Override
    public TokenEntity save(TokenEntity token) {
        return repository.save(token);
    }

    @Override
    public TokenEntity getToken(String token) {
        return repository.findByToken(token);
    }

    @Override
    public void delete(TokenEntity token) {
        repository.delete(token);
    }

    @Override
    public TokenEntity createToken(UserEntity user) {
        TokenEntity token = new TokenEntity();
        token.setUser(user);
        token.setDate(new Date());
        token.setToken(UUID.randomUUID().toString());

        save(token);

        return token;

    }

    @Override
    public boolean validateToken(TokenEntity token) {
        Calendar cal = Calendar.getInstance();
        if ((cal.getTime().getTime()) - token.getDate().getTime() <= Integer.parseInt(env.getRequiredProperty("token.time"))){
            repository.delete(token);
            return true;
        }
        else return false;
    }
}
