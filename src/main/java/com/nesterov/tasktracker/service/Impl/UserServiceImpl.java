package com.nesterov.tasktracker.service.Impl;

import com.nesterov.tasktracker.entity.TokenEntity;
import com.nesterov.tasktracker.entity.UserEntity;
import com.nesterov.tasktracker.repository.UserRepository;
import com.nesterov.tasktracker.service.TokenService;
import com.nesterov.tasktracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<UserEntity> getAll() {
        return repository.findAll();
    }

    public UserEntity getById(long id) {
        return repository.findById(id).get();
    }

    public UserEntity save(UserEntity user) {
        return repository.saveAndFlush(user);
    }

    public void delete(long id) {
        repository.deleteById(id);
    }

    public UserEntity getByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public void confirmRegistration(TokenEntity token) {
        UserEntity user = token.getUser();
        user.setEnabled(true);
        save(user);
        tokenService.delete(token);
    }

    @Override
    public boolean checkUserInDB(String login) {
        return userService.getByEmail(login) != null;
    }

    @Override
    public UserEntity saveNewUser(UserEntity user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return save(user);
    }
}
