package com.nesterov.tasktracker.service.Impl;

import com.nesterov.tasktracker.entity.UserEntity;
import com.nesterov.tasktracker.service.EmailService;
import com.nesterov.tasktracker.service.TokenService;
import com.nesterov.tasktracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
@Transactional
@PropertySource("classpath:email.properties")
public class EmailServiceImpl implements EmailService {

    @Resource
    private Environment env;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @Override
    public boolean sendEmail(String email, String text) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", env.getRequiredProperty("mail.smtp.auth"));
        props.put("mail.smtp.starttls.enable", env.getRequiredProperty("mail.smtp.starttls.enable"));
        props.put("mail.smtp.host", env.getRequiredProperty("mail.smtp.host"));
        props.put("mail.smtp.port", env.getRequiredProperty("mail.smtp.port"));

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(env.getRequiredProperty("email.mainEmail"), env.getRequiredProperty("email.mainPass"));
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(env.getRequiredProperty("email.mainEmail")));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject(env.getRequiredProperty("email.subject"));
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean sendRegistrationEmail(UserEntity user) {
        String text = String.format(env.getRequiredProperty("email.sendRegistrationEmail"),
                user.getFirstName(),tokenService.createToken(user).getToken());
        return sendEmail(user.getEmail(), text);
    }

    @Override
    public boolean sendNewPasswordEmail(UserEntity user) {
        UserEntity userEntity = userService.getByEmail(user.getEmail());
        String text = String.format(env.getRequiredProperty("email.sendNewPasswordEmail"),
                userEntity.getFirstName(),tokenService.createToken(userEntity).getToken());
        return sendEmail(userEntity.getEmail(), text);
    }
}
