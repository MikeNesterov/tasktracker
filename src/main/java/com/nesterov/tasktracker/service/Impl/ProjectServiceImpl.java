package com.nesterov.tasktracker.service.Impl;

import com.nesterov.tasktracker.entity.ProjectEntity;
import com.nesterov.tasktracker.repository.ProjectRepository;
import com.nesterov.tasktracker.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Set<ProjectEntity> getAllByManager(long user_id) {
        return projectRepository.getAllByManager_Id(user_id);
    }
}
