package com.nesterov.tasktracker.service;


import com.nesterov.tasktracker.entity.UserEntity;

public interface EmailService {
    boolean sendEmail(String email, String text);
    boolean sendRegistrationEmail(UserEntity user);
    boolean sendNewPasswordEmail(UserEntity user);
}
