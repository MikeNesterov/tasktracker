package com.nesterov.tasktracker.controller;


import com.nesterov.tasktracker.entity.ProjectEntity;
import com.nesterov.tasktracker.security.MyUserPrincipal;
import com.nesterov.tasktracker.service.ProjectService;
import com.nesterov.tasktracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class ProjectController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    @GetMapping("/api/projects")
    public Set<ProjectEntity> listAllQuestions(@AuthenticationPrincipal MyUserPrincipal myUserPrincipal) {
        Set<ProjectEntity> projects = projectService.getAllByManager(myUserPrincipal.getUserId());

        return projects;
    }
}
