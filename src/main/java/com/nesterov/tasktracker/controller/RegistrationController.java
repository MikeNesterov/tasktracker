package com.nesterov.tasktracker.controller;

import com.nesterov.tasktracker.entity.TokenEntity;
import com.nesterov.tasktracker.entity.UserEntity;
import com.nesterov.tasktracker.service.EmailService;
import com.nesterov.tasktracker.service.TokenService;
import com.nesterov.tasktracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.validation.Valid;

@Controller
@PropertySource("classpath:messages.properties")
public class RegistrationController {

    @Resource
    private Environment env;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(ModelMap model) {
        UserEntity user = new UserEntity();
        model.addAttribute("user", user);
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@Valid UserEntity user, BindingResult result) {

        if (userService.checkUserInDB(user.getEmail())) {
            result.addError(new FieldError("user", "email", env.getRequiredProperty("failEmail")));
        }

        if (result.hasErrors()) {
            return "registration";
        }

        userService.saveNewUser(user);
        emailService.sendRegistrationEmail(user);

        return "redirect:/login?email="+user.getEmail();
    }

    @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
    public String confirmRegistration
            (Model model, @RequestParam("token") String token) {

        TokenEntity tokenEntity = tokenService.getToken(token);
        if (tokenEntity == null) {
            model.addAttribute("message", env.getRequiredProperty("token.invalidlink"));
            return "badUser";
        }


        if (!tokenService.validateToken(tokenEntity)) {
            model.addAttribute("message", env.getRequiredProperty("token.timeout"));
            return "badUser";
        }

        userService.confirmRegistration(tokenEntity);

        return "redirect:/login";
    }
}
