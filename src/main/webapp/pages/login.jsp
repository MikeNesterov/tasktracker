<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>User Login Form</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
</head>

<body>

<div class="form-container">

    <h1>Login</h1>
    <c:url var="loginUrl" value="/login"/>
    <form action="${loginUrl}" method="post" class="form-horizontal">
        <c:if test="${param.error != null}">
            <div class="alert alert-danger">
                <p>Invalid username and password.</p>
            </div>
        </c:if>
        <c:if test="${param.logout != null}">
            <div class="alert alert-success">
                <p>You have been logged out successfully.</p>
            </div>
        </c:if>
        <c:if test="${param.email != null}">
            <div class="alert alert-success">
                <p>We have also sent you a confirmation mail to your email address : ${param.email}</p>
            </div>
        </c:if>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="username">Email</label>
                <div class="col-md-7" class="form-control input-sm">
                    <input type="text" class="form-control" id="username" name="username"
                           placeholder="Enter Username" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="password">Password</label>
                <div class="col-md-7">
                    <input type="password" class="form-control" id="password" name="password"
                           placeholder="Enter Password" required>

                </div>
            </div>
        </div>


        <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>

        <div class="row">
            <div class="form-actions floatRight">
                <input type="submit" value="Login" class="btn btn-primary btn-sm">
            </div>
        </div>
        <br/>
        <div class="hyperlink"><a href="/registration">Registration</a></div>
    </form>
</div>
</body>
</html>