<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<div>
    <security:authorize access="hasRole('ROLE_USER')">
        User: <security:authentication property="principal.username" />
        <a href = "<c:url value = "/logout"/>">Logout</a>
    </security:authorize>
</div>
<br/>
