package com.nesterov.tasktracker.test.service;

import com.nesterov.tasktracker.config.DatabaseConfig;
import com.nesterov.tasktracker.config.WebConfig;
import com.nesterov.tasktracker.entity.UserEntity;
import com.nesterov.tasktracker.repository.ProjectRepository;
import com.nesterov.tasktracker.repository.TaskRepository;
import com.nesterov.tasktracker.service.Impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfig.class, WebConfig.class})
@WebAppConfiguration
public class UserRepositoryTest  {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private UserServiceImpl service;

    @Resource
    private ProjectRepository projectRepository;

    @Resource
    private TaskRepository taskRepository;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
    }

    @Test
    public void testSaveUser() throws Exception {
        UserEntity user = new UserEntity();

        user.setEmail("111");
        user.setFirstName("F");
        user.setLastName("L");
        user.setPassword("111");

        service.save(user);


        Assert.assertEquals(1, 1);

    }
}
